#!/usr/bin/env python3
# Copyright (c) 2018 Sampo Sorsa <sorsasampo@protonmail.com>
import argparse
import urllib.parse
import urllib.request

from lxml.html import parse


def get_conversion(amount, src, dst):
    params = urllib.parse.urlencode({
        'a': args.amount,
        'from': args.src,
        'to': args.dst
    })
    url = 'https://finance.google.com/finance/converter?%s' % params
    with urllib.request.urlopen(url) as f:
        doc = parse(f).getroot()
    e_result = doc.get_element_by_id('currency_converter_result')
    result = str(e_result.text_content()).strip()
    return result


parser = argparse.ArgumentParser(
    description='Currency converter using Google Finance API',
    epilog='Example: %(prog)s 20 usd eur'
)

parser.add_argument('amount', type=float)
parser.add_argument('src', help='Currency to convert from', metavar='from')
parser.add_argument('dst', help='Currency to convert to', metavar='to')

if __name__ == '__main__':
    args = parser.parse_args()
    result = get_conversion(args.amount, args.src, args.dst)
    print(result)
